# leetcode

My solutions to leetcode questions, along with explantory information.

Why?

For personal reference, and also for showing what I know.

Everything is organized by `learning path` -> `problem` -> `language` or, if it's a non-pathed problem, by `category` -> `problem` -> `language`:

```txt
leetcode
|- top-interview-questions-easy
    |- array
        |- remove-duplicates-from-sorted-array
            |- remove-duplicates-from-sorted-array.js
            |- remove-duplicates-from-sorted-array-js.md
            |- remove-duplicates-from-sorted-array.py
            |- remove-duplicates-from-sorted-array-py.md
            |- ... other languages?!?!
```
